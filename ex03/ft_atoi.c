/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/09/24 17:42:54 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//#include <stdio.h>

int	ft_atoi(char *str)
{
	int	index;
	int	sign;
	int	returned;

	index = 0;
	returned = 0;
	sign = 0;
	while ((str[index] > 8 && str[index] < 14) || str[index] == 32)
		index++;
	while (str[index] == 43 || str[index] == 45)
	{
		if (str[index] == 45)
			sign++;
		index++;
	}
	while (str[index] >= '0' && str[index] <= '9')
	{
		returned *= 10;
		returned += str[index] - 48;
		index++;
	}
	if (sign % 2 == 1)
		returned *= -1;
	return (returned);
}

/*int	main(void)
{
	printf("%d\n",ft_atoi("	---+--+1234ab567"));
}*/
