/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/09/25 17:20:13 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	positive(long *num)
{
	if (*num < 0)
	{
		write (1, "-", 1);
		*num *= -1;
	}
}

void	writef(int *basen, long *temp, unsigned char *base, long *num)
{
	while (*temp > 0)
	{
		write(1, &base[*num / *temp % *basen], 1);
		*temp /= *basen;
	}
}

void	ft_putnbr_base(int nbr, char *base)
{
	int		bn;
	long	nbr2;
	long	t[2];

	bn = 0;
	t[0] = 1;
	nbr2 = nbr;
	while (base[bn])
	{
		t[1] = bn + 1;
		while (base[t[1]++ - 1])
			if (base[t[1] - 1] == base[bn] || base[bn] == 43 || base[bn] == 45)
				return ;
		bn++;
	}
	if (bn <= 1)
		return ;
	positive(&nbr2);
	t[1] = nbr2;
	while (t[1] >= bn && t[0]++)
		t[1] /= bn;
	t[1] = 1;
	while (t[0]-- > 1 && bn > 1)
		t[1] *= bn;
	writef(&bn, &t[1], (unsigned char *)base, &nbr2);
}

/*int	main(void)
{
	ft_putnbr_base(-2147483648, "01");
	write (1, "\n", 1);
	ft_putnbr_base(-2147483648, "0123456789");
	write (1, "\n", 1);
	ft_putnbr_base(-2147483648, "0123456789ABCDEF");
	write (1, "\n", 1);
	ft_putnbr_base(-2147483648, "-123456789");
	write (1, "\n", 1);
	ft_putnbr_base(-2147483648, "");
	write (1, "\n", 1);
	ft_putnbr_base(0, "poneyvif");
	write (1, "\n", 1);
	ft_putnbr_base(102509, "");
	write (1, "\n", 1);
	ft_putnbr_base(102509, "1123456789ABCDEF");
	write (1, "\n", 1);
	ft_putnbr_base(102509, "+123456789ABCDEF");
	write (1, "\n", 1);
}*/
