/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/09/24 17:42:54 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int	checkbase(char *base)
{
	int	i;
	int	i2;

	i = 0;
	while (base[i])
	{
		i2 = i + 1;
		while (base[i2++ - 1])
			if (base[i2 - 1] == base[i] || base[i] == '+' || base[i] == '-')
				return (0);
		i++;
	}
	if (i <= 1)
		return (0);
	return (1);
}

int	fn(char c, char *base)
{
	int	i;

	i = 0;
	while (base[i] != c)
	{
		if (base[i] == '\0')
			return (-1);
		i++;
	}
	return (i);
}

int	ft_strlen(char *str)
{
	int	index;

	index = 0;
	while (str[index])
		index++;
	return (index);
}

int	ft_atoi_base(char *str, char *base)
{
	int	i;
	int	returned;
	int	sign;

	i = 0;
	sign = 0;
	returned = 0;
	if (checkbase(base) == 0)
		return (0);
	while ((str[i] > 8 && str[i] < 14) || str[i] == 32)
		i++;
	while ((str[i] == 43 || str[i] == 45) && i++)
		if (str[i - 1] == 45)
			sign++;
	while (i < ft_strlen(str) - 1 && str[i] && fn(str[i], base) != -1)
	{
		returned += fn(str[i], base);
		if (++i && fn(str[i], base) == -1)
			break ;
		returned *= ft_strlen(base);
	}
	if (fn(str[i], base) != -1)
		returned += fn(str[i], base);
	if (sign % 2 == 1)
		returned *= -1;
	return (returned);
}

/*int	main(void)
{
	printf("%d\n", ft_atoi_base("DEADBE", "0123456789ABCDEF"));
}*/
